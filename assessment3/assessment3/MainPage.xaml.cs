﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using assessment3.Classes;
using Windows.UI.Popups;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace assessment3
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            names.ItemsSource = loadAllTheData();
        }

        // Alternate the state of the SplitView pane
        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }
        
        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}'";
            var b = MySQL.ShowInList(command);

            userAccount.Text = b[0];
            fname.Text = b[1];
            lname.Text = b[2];
            dob.Text = b[3];
            str_num.Text = b[4];
            str_name.Text = b[5];
            postcode.Text = b[6];
            city.Text = b[7];
            phone1.Text = b[8];
            phone2.Text = b[9];
            email.Text = b[10];

        }

        // Return all accounts stored in the 'tbl_people' table in the DB
        static List<string> loadAllTheData()
        {
            var command = $"Select FNAME from tbl_people";

            var list = MySQL.ShowInList(command);

            return list;
        }

        // Clear all form fields
        void clearAll()
        {
            userAccount.Text = "";
            fname.Text = "";
            lname.Text = "";
            dob.Text = "";
            str_num.Text = "";
            str_name.Text = "";
            postcode.Text = "";
            city.Text = "";
            phone1.Text = "";
            phone2.Text = "";
            email.Text = "";
            
        }

        // Error handling
        static async void messageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Error!";
            dialog.Content = "You got issues!";

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });

            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
                { }
        }

        // Update account fields with new selected user
        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(names.SelectedItem != null)
            {
                selectedName(names.SelectedItem.ToString());
            }
        }

        // Add new account to DB
        private void addToDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var strnum = Convert.ToInt32(str_num.Text);

                MySQL.AddData(fname.Text, lname.Text, dob.Text, strnum, str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            } catch (Exception ex)
            {
                messageBox(ex.Message);
            }
        }

        // Update data in DB
        private void updateDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MySQL.UpdateData(fname.Text, lname.Text, dob.Text, Convert.ToInt32(str_num.Text), str_name.Text, postcode.Text, city.Text, phone1.Text, phone2.Text, email.Text, userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
            catch (Exception ex)
            {
                messageBox(ex.Message);
            }
        }

        // Delete account from DB
        private void removeFromDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MySQL.DeleteData(userAccount.Text);
                names.ItemsSource = loadAllTheData();
                clearAll();
            }
            catch (Exception ex)
            {
                messageBox(ex.Message);
            }
        }
    }
}